package no.uib.inf101.v23.lecture7;

import java.util.Random;

public class RockPokemon implements IPokemon{

	static Random random = new Random();

	String name;
	int healthPoints;
	int maxHealtPoints;
	int strength;

	RockPokemon(String name, int maxHealtPoints, int strength) {
		this.name = name;
		if(maxHealtPoints<=0) {
			throw new IllegalArgumentException("Pokemon must have positive HP");
		}
		this.maxHealtPoints = maxHealtPoints;
		this.strength = strength;
		healthPoints = maxHealtPoints;
	}

	RockPokemon(String name){
		this(name,getRandomHP(),getRandomStrength());
	}

	static int getRandomStrength() {
		return 20+random.nextInt(10);
	}

	static int getRandomHP() {
		return 100+random.nextInt(10);
	}


	public String getName() {
		return name;
	}

	@Override
	public int getStrength() {
		return strength;
	}

	@Override
	public int getCurrentHP() {
		return healthPoints;
	}

	@Override
	public int getMaxHP() {
		return maxHealtPoints;
	}

	public boolean isAlive() {
		return healthPoints>0;
	}

	@Override
	public int attack(IPokemon target) {
		int damageInflicted = this.getStrength()-target.getStrength();
		return target.damage(damageInflicted);
	}

	@Override
	public int damage(int damageTaken) {
		damageTaken = Math.max(damageTaken/2, 0);
		damageTaken = Math.min(damageTaken, getCurrentHP());
		healthPoints-=damageTaken;
		return damageTaken;
	}

	@Override
	public String toString() {
		return name+" HP: ("+getCurrentHP()+"/"+getMaxHP()+") STR: "+getStrength();
	}
	void rock() {
		System.out.println("Yeah yeah!");
	}
}
